using UnityEngine;

public class CameraController : MonoBehaviour
{

    public Transform target;  // El objeto que la c�mara debe seguir
    public Vector3 offset;    // La distancia entre la c�mara y el objeto que sigue

    void LateUpdate()
    {
        transform.position = target.position + offset;
        transform.LookAt(target);
    }
}